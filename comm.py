#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
from gevent import monkey
monkey.patch_all()
import os
import requests
import json

__author__ = "Stelios Sfakianakis <ssfak@ics.forth.gr>"

__all__ = ['submit_workflow', 'get_status', 'get_outputs', 'submit_workflow_source']

SERVER = os.environ.get('CROMWELL_SERVER')

if SERVER.endswith('/'):
    SERVER += "api/workflows/v1"
else:
    SERVER += "/api/workflows/v1"


def submit_workflow(wfurl, inputs):
    """
    Submits the URL of a workflow and the set of inputs.
    """
    r = requests.post(SERVER, files={'workflowUrl': wfurl,
                                     'workflowInputs': inputs,
                                     'workflowType': 'CWL',
                                     'workflowTypeVersion': 'v1.0'})
    if not r.ok:
        print("Cromwell returned " + str(r.status_code))
        print(r.text)
    r.raise_for_status()
    return r.json()['id']


def submit_workflow_source(wfsrc, inputs):
    """
    Submits the workflow given as a 'source' (i.e. by value),
    and the set of inputs.
    """
    r = requests.post(SERVER, files={'workflowSource': wfsrc,
                                     'workflowInputs': inputs,
                                     'workflowType': 'CWL',
                                     'workflowTypeVersion': 'v1.0'})
    r.raise_for_status()
    return r.json()['id']


def get_status(wfid):
    """
    Queries the server for the status of the workflow with the
    given id.
    :param str wfid: Workflow run id
    """
    url = SERVER + '/' + wfid + '/status'
    r = requests.get(url)
    if not r.ok:
        print("Cromwell returned " + str(r.status_code))
        print(r.text)
    r.raise_for_status()
    status = r.json()['status']
    return status.lower()


def get_outputs(runid):
    """
    Retieves the description of the run's outputs given its id.
    :param str runid: Workflow run id
    """
    url = SERVER + '/' + runid + '/metadata'
    r = requests.get(url)
    if not r.ok:
        print("Cromwell returned " + str(r.status_code))
        print(r.text)
    r.raise_for_status()
    return r.json()['outputs']
