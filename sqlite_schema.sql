
CREATE TABLE IF NOT EXISTS run(
	runid VARCHAR PRIMARY KEY,
	status VARCHAR NOT NULL DEFAULT 'submitted',
	description VARCHAR NOT NULL DEFAULT '',
	created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	status_updated_at DATETIME,
	wfurl VARCHAR,
	inputs VARCHAR,
	outputs VARCHAR,
	deleted boolean DEFAULT false
);