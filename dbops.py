#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
import sqlite3

DBFILE = 'ui.db'

__all__ = ['connection', 'allRuns', 'allUnfinishedRuns',
           'updateFinishedRun', 'updateRunStatus']


def connection():
    con = sqlite3.connect(DBFILE)
    con.row_factory = sqlite3.Row
    con.text_factory = str
    return con


def allUnfinishedRuns(db):
    rows = db.execute("""SELECT runid, status, description,
     datetime(strftime('%s', created_at), 'unixepoch', 'localtime') cd
     FROM run
     WHERE status <> 'failed' and status <> 'succeeded'""").fetchall()
    data = [{'rid': r['runid'],
             'st': r['status'],
             'description': r['description'],
             'd': r['cd']}
            for r in rows]
    return data


def getRun(db, runid):
    r = db.execute("""SELECT runid, status, description,
     datetime(strftime('%s', created_at), 'unixepoch', 'localtime') cd,
     inputs, outputs
     FROM run WHERE runid=?""", [runid]).fetchone()
    data = {'rid': r['runid'],
            'st': r['status'],
            'description': r['description'],
            'ins': r['inputs'],
            'outs': r['outputs'],
            'd': r['cd']}
    return data


def updateRunStatus(db, runid, status):
    db.execute("""UPDATE run set status_updated_at=datetime('now'),
               status=? WHERE runid=?""", [status, runid])


def updateFinishedRun(db, runid, status, outputs):
    db.execute("""UPDATE run set status_updated_at=datetime('now'),
               status=? , outputs=? WHERE runid=?""",
               [status, outputs, runid])


def allRuns(db):
    rows = db.execute("""SELECT runid, status, description,
     datetime(strftime('%s', created_at), 'unixepoch', 'localtime') cd
     FROM run ORDER BY created_at DESC""").fetchall()
    data = [{'rid': r['runid'],
             'st': r['status'],
             'description': r['description'],
             'd': r['cd']}
            for r in rows]
    return data
