#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
from __future__ import print_function
from gevent import monkey
monkey.patch_all()
import gevent
import bottle
from bottle.ext import sqlite
from bottle import get, post, route, request, response, redirect, static_file, hook
from bottle import jinja2_view as view, jinja2_template as template
import json
import beaker.middleware
import comm
import dbops
from cromlet import MasterCromlet

__author__ = "Stelios Sfakianakis <ssfak@ics.forth.gr>"

session_opts = {
    'session.type': 'file',
    'session.data_dir': './session/',
    'session.auto': True,
}

StatusPoller = MasterCromlet()
# StatusPoller.start()
# StatusPoller.pollStart('f6961017-a1b8-43bc-9582-8b97958611c8')


@get('/')
@view('templates/home.html')
def index():
    return {}


@get('/launch')
@view('templates/launch.html')
def launch_wf():
    return {}


@get('/history')
@view('templates/history.html')
def show_history():
    return {}


@post('/launch')
def do_launch_wf(db):
    url = request.forms.get('wfurl')
    description = request.forms.get('description')
    inputs = request.forms.get('inputs')
    runid = comm.submit_workflow(url, inputs)
    StatusPoller.pollStart(runid)
    request.session['flash'] = "Run {} created!".format(runid)
    db.execute('INSERT INTO run(runid,wfurl,inputs,description) VALUES(?,?,?,?)',
               [runid, url, inputs, description])
    redirect('/launch')


@get('/run/<runid>')
def get_status_endpoint(runid, db):
    # return comm.get_status(runid)
    row = db.execute('SELECT * FROM run WHERE runid=?', [runid]).fetchone()
    data = dbops.getRun(db, runid)
    response.content_type = 'text/json'
    return json.dumps(data)


@get('/history.json')
def history_json(db):
    response.content_type = 'text/json'
    request.session['flash'] = None
    # return comm.get_status(runid)
    data = dbops.allRuns(db)
    for r in data:
        r['m'] = StatusPoller.workers.get(r['rid']) is not None
    return json.dumps({'data': data})


def sse_response(runid, status):
    res = {'rid': runid, 'st': status}
    return 'data:' + json.dumps(res, separators=(',', ':'))+'\n\n'


def sse_ping():
    return ":\n\n"


@get('/status/<runid>')
def get_status_stream2(runid):
    response.content_type = 'text/event-stream'
    response.cache_control = 'no-cache'
    q = gevent.queue.Queue()
    StatusPoller.addMonitor(q)
    StatusPoller.pollStart(runid)
    gevent.getcurrent().link(lambda x: StatusPoller.removeMonitor(q))
    while True:
        try:
            mesg = q.get(timeout=5)
            if mesg is StopIteration:
                return
            rid, status = mesg
            if rid == runid:
                yield sse_response(runid, status)
        except gevent.queue.Empty:
            yield sse_ping()


@get('/monitor')
def monitor():
    response.content_type = 'text/event-stream'
    response.cache_control = 'no-cache'
    q = gevent.queue.Queue()
    StatusPoller.addMonitor(q)
    gevent.getcurrent().link(lambda x: StatusPoller.removeMonitor(q))
    while True:
        try:
            mesg = q.get(timeout=5)
            if mesg is StopIteration:
                return
            else:
                rid, status = mesg
                yield sse_response(rid, status)
        except gevent.queue.Empty:
            yield sse_ping()


@get('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='./assets')


@hook('before_request')
def setup_request():
    request.session = request.environ['beaker.session']


app = bottle.app()
app.debug = True
plugin = sqlite.Plugin(dbfile=dbops.DBFILE)
app.install(plugin)
app = beaker.middleware.SessionMiddleware(bottle.app(), session_opts)


# Update the status in the local DB for all workflow
# runs that we don't have up-to-date information:
with dbops.connection() as con:
    rs = dbops.allUnfinishedRuns(con)
    for r in rs:
        StatusPoller.pollStart(r['rid'])

if __name__ == '__main__':
    dir(app)
    bottle.run(app=app, host='0.0.0.0', port=8080, server='gevent')
