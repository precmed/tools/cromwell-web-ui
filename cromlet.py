#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
import gevent
from gevent import Greenlet
from gevent.event import Event
from gevent.queue import Queue
from comm import get_status, get_outputs
import dbops
import json

__author__ = "Stelios Sfakianakis <ssfak@ics.forth.gr>"

__all__ = ['MasterCromlet']


DEFAULT_TIMEOUT = 15


class MasterCromlet(Greenlet):
    """
    """

    def __init__(self, timeout=DEFAULT_TIMEOUT):
        Greenlet.__init__(self)
        self.timeout = timeout
        self.workers = dict()
        self.masterq = Queue()
        self.monitors = set()
        self.stopped = False
        Greenlet.start(self)

    def pollStart(self, runid):
        worker = self.workers.get(runid)
        if not worker:
            self._spawnWorker(runid)

    def _spawnWorker(self, runid):
        worker = Cromlet(runid, self, self.timeout)
        worker.link(lambda x: self._onWorkerCompletion(x))
        self.workers[runid] = worker
        gevent.spawn_later(1, lambda: worker.start())

    def _onWorkerCompletion(self, worker):
        rid = worker.runId()
        del self.workers[rid]
        print("Worker for {} finished, ex={}".format(rid, worker.exc_info))
        if not worker.successful():
            self._spawnWorker(rid)

    def addMonitor(self, q):
        self.monitors.add(q)

    def removeMonitor(self, q):
        if q in self.monitors:
            self.monitors.remove(q)

    def newStatus(self, runid, status):
        self.masterq.put([runid, status])

    def _run(self):
        while not self.stopped:
            mesg = self.masterq.get()
            if mesg is StopIteration:
                continue
            for q in self.monitors:
                q.put_nowait(mesg)
            # Update the information in the local db!
            rid, status = mesg
            with dbops.connection() as con:
                if status == 'succeeded':
                    outs = get_outputs(rid)
                    dbops.updateFinishedRun(con, rid, status, json.dumps(outs))
                else:
                    dbops.updateRunStatus(con, rid, status)


class Cromlet(Greenlet):
    """
    A Greenlet for retrieving the status of a running workflow
    from Cromwell
    """

    def __init__(self, runid, master, timeout=DEFAULT_TIMEOUT):
        Greenlet.__init__(self)
        self.runid = runid
        self.master = master
        self.timeout = timeout
        self._ev = Event()
        self.checking = False
        self.stopped = False
        self.run_status = None

    def runId(self):
        return self.runid

    def runStatus(self):
        return self.run_status

    def __hash__(self):
        return hash(self.runId())

    def __repr__(self):
        rid = self.runid
        return "Cromlet-{}({})".format(rid, self.run_status)

    def wakeup(self):
        self._ev.set()

    def stop(self):
        print("{} stopping...".format(str(self)))
        self.stopped = True
        self.wakeup()

    def _run(self):
        rid = self.runid
        print("{} running...".format(str(self)))
        while not self.stopped:
            print("{} polling...".format(str(self)))
            try:
                status = get_status(rid)
                if status != self.run_status:
                    self.run_status = status
                    self.master.newStatus(rid, status)
                if status == 'failed' or status == 'succeeded':
                    break
            except Exception as ex:
                print("Exception: {}".format(str(ex)))
                return
            print("{} Going to sleep for {} secs ...".format(str(self), self.timeout))
            op = self._ev.wait(timeout=self.timeout)
            if op:
                self._ev.clear()
                if self.stopped:
                    break
            gevent.sleep(self.timeout)
        print("{} finished...".format(str(self)))
