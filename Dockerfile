FROM python:slim

RUN mkdir -p /cromwell-web-ui/templates
WORKDIR /cromwell-web-ui
COPY requirements.txt requirements.txt
COPY *.py /cromwell-web-ui/
COPY templates/*.html /cromwell-web-ui/templates/
COPY ui.db /cromwell-web-ui/
RUN pip install --upgrade -r requirements.txt

EXPOSE 8080/tcp
ENTRYPOINT ["python3", "cromwell_ui.py"]
