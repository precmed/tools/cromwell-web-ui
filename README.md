## A web UI for Cromwell

This is a web application for launching the execution of CWL workflows in Cromwell. The backend is developed in Python, see `requirements.txt` for the Python packages needed.

The following shows the user interface for submitting a workflow to run:

![](images/launch.png)

You need to provide the URL for retrieving the CWL description of the workflow, the inputs in YAML format, and optionally a description so that later on you can have some information about this run.

The history panel shows all past executions and their status, and allows filtering (show only "today's" runs or the runs of specific status):

![](images/results-filter.jpg)

When launching a run, the server side continuously monitors its execution and the user is notified whenever a run fails or succeeds. The notifications can be "desktop notifications" if the user enables them, otherwise they are shown inside the web page, as shown in the next screenshot:

![](images/notification.jpg)

By double clicking a row in the results' table, you get information about its inputs and outputs:

![](images/run-details.jpg)


## Docker

You can use the given `Dockerfile` to build a Docker image. It is based on the official [Python Docker images](https://hub.docker.com/_/python) and more specifically its [slim](https://github.com/docker-library/python/blob/master/3.7/stretch/slim/Dockerfile) flavor.

You can build it (in the dicrectory containing the `Dockerfile` and the rest sources) as follows (for example):

```bash
docker build -t sgsfak/slim-cromwell-ui .
```

and then run it, exposing the 8080 port:

```bash
docker run --rm -p 8080:8080 sgsfak/slim-cromwell-ui
```

If you want to change the Cromwell URL used, you can pass it as an Environment variable, e.g.

```shell
docker run --rm -p 8080:8080 -e CROMWELL_SERVER=http://my-cromwell-server.org sgsfak/slim-cromwell-ui
```



